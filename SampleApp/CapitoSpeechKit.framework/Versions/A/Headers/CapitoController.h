//
//  CapitoController.h
//  (C) Capito Systems Ltd 2013, All Rights Reserved.
//
//  This class is the device API for communicating
//  with the Capito Cloud
//
//  Created by Darren Harris on 3/14/13.
//
//

#import <Foundation/Foundation.h>
#import "CapitoResponse.h"
#import "CapitoTranscription.h"

#define HOST                            @"host"
#define PATH                            @"path"
#define USESSL                          @"useSSL"
#define MODE                            @"mode"
#define LANGUAGE                        @"lang"
#define APPVERSION                      @"appVersion"
#define TESTCASE                        @"testCase"
#define EVENT                           @"eventType"
#define VOICEPROMPT                     @"voicePrompt"
#define VOICESTART                      @"voiceStart"
#define VOICESTOP                       @"voiceStop"
#define VOICECANCEL                     @"voiceCancel"
#define kCAPModeArray                   @"SR", @"T", @"L", @"M", nil
#define kCAPEventTypeArray              @"NAV", @"SEL", @"DSEL", @"TRAN", @"SRCH", @"COMP", @"LOG", nil
#define kCAPCommandTypeArray            @"TCH", @"TXT", @"VOX", nil

@protocol SpeechDelegate <NSObject>
@required
- (void) speechControllerDidBeginRecording;
- (void) speechControllerDidFinishRecording;
- (void) speechControllerProcessing:(CapitoTranscription *)transcription;
- (void) speechControllerDidFinishWithResults:(CapitoResponse *)response;
- (void) speechControllerDidFinishWithError:(NSError *)error;
@end

@protocol TouchDelegate <NSObject>
@required
- (void) touchControllerDidFinishWithResults:(CapitoResponse *)response;
- (void) touchControllerDidFinishWithError:(NSError *)error;
@end

@protocol TextDelegate <NSObject>
@required
- (void) textControllerDidFinishWithResults:(CapitoResponse *)response;
- (void) textControllerDidFinishWithError:(NSError *)error;
@end

/*!
 @abstract Type for the Capito Command Type
 */
typedef NSUInteger CAPCommandType;

enum CAPCommandType {
    CAPTouchCommand = 0,
    CAPTextCommand = 1,
    CAPVoiceCommand = 2,
    CAPConnectCommand = 3
};

/*!
 @abstract Type for the Capito Voice/CAPTouch Operations Mode
 */
typedef NSUInteger CAPModeType;

enum CAPModeType {
    CAPModeStaticResponse = 0,
    CAPModeTest = 1,
    CAPModeLive = 2,
    CAPModeManual = 3
    };

/*!
 @abstract Type for the Capito Event Type
 */
typedef NSUInteger CAPEventType;

enum CAPEventType {
    CAPNavigate = 0,
    CAPSelect = 1,
    CAPUnSelect = 2,
    CAPTransact = 3,
    CAPSearch = 4,
    CAPComplete = 5,
    CAPLog = 6
};



@interface CapitoController : NSObject

//possible connect return values
extern NSString * const kCapitoConnect_SUCCESS;
extern NSString * const kCapitoConnect_EXPIRED;
extern NSString * const kCapitoConnect_UNAVAILABLE;
extern NSString * const kCapitoConnect_NOTSETUP;

+ (CapitoController *) getInstance;


/*!
 set-up method for initialising the Capito CAPSpeech Kit
 */
- (void) setupWithID:(NSString*) ID
                host:(NSString*) host
                port:(NSNumber*) port
              useSSL:(BOOL) ssl;

+ (CAPEventType) eventType:(NSString *)event;
+ (CAPModeType) modeType:(NSString *)mode;
+ (CAPCommandType) commandType:(NSString *)command;

- (void) pushToTalk: (id <SpeechDelegate>)speechDelegate withDialogueContext:( NSDictionary *)context;
- (void) cancelTalking;
- (void) confirm:(CAPCommandType)type isCorrect:(BOOL)correct;
- (BOOL) isLastEventTouch;
- (BOOL) isLastEventVoice;
- (BOOL) isLastEventText;

- (void)        text: (id <TextDelegate>)textDelegate
               input: (NSString *)text
 withDialogueContext:( NSDictionary *)context;

- (void)    touch: (id <TouchDelegate>)touchDelegate
        eventType: (CAPEventType)eventType
            event: (NSString *)event
  dialogueContext: (NSDictionary *)context
     withResponse: (BOOL)_expected;

- (void)    touch: (id <TouchDelegate>)touchDelegate
        eventType: (CAPEventType)eventType
            event: (NSString *)event
  dialogueContext: (NSDictionary *)context;

- (float) audioLevel;

- (void) textToSpeech: (NSString *)text;
- (void) cancelTextToSpeech;


- (NSString *) connect;

- (void) disconnect;

/*!
 coords is a dictionary with <NSString> key and <NSString> value
 e.g.       lat = 0;
            long = 0;
 */
- (void) setGeoLocation:(NSDictionary *)coords;

@end
