//
//  FTViewController.h
//  FastTrains
//
//  Created by Darren Harris on 18/02/14.
//  Copyright (c) 2014 Capito Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CapitoSpeechKit/CapitoController.h>
#import "ActivityView.h"
#import "MeterView.h"


@interface CAPViewController : UIViewController <SpeechDelegate, TouchDelegate, TextDelegate, UISearchBarDelegate> {
    BOOL isRecording;
    CapitoController *controller;
}

@property (strong, nonatomic) IBOutlet UIButton *microphone;
@property (nonatomic, retain) ActivityView* activityView;
@property (strong, nonatomic) IBOutlet MeterView *meterView;
@property (strong, nonatomic) IBOutlet UITextView *transcriptionView;
@property (weak, nonatomic) IBOutlet UIButton *info;
@property (weak, nonatomic) IBOutlet UIButton *settings;
@property (weak, nonatomic) IBOutlet UISearchBar *textControlBar;
@property (weak, nonatomic) IBOutlet UIButton *textControl;
@property (weak, nonatomic) IBOutlet UITextView *infoText;

- (IBAction)onMicrophoneClick:(id)sender;
- (IBAction)onTextControlClick:(id)sender;
- (IBAction)onInfoClick:(id)sender;

@end