//
//  MeterView.m
//
//
//  Created by Darren Harris 17/10/2013
//  Copyright (c) 2013 Capito Systems Ltd. All rights reserved.
//

#import "MeterView.h"

@implementation MeterView

-(void)initialise{
    self.soundMeters = [[NSMutableArray alloc] initWithCapacity:40];
    
    for(int i=0; i<SOUND_METER_COUNT; i++) {
        self.soundMeters[i] = [NSNumber numberWithInteger:-25];
    }
    
    self.offset = [NSNumber numberWithInteger:0];//65
    
    bool isiPhone5 = CGSizeEqualToSize([[UIScreen mainScreen] preferredMode].size,CGSizeMake(640, 1136));
    
    if(isiPhone5)
        self.offset = [NSNumber numberWithInteger:0]; //108
        
}

-(void)awakeFromNib{
    NSLog(@"MeterView:awakeFromNib");
    [self initialise];
    [super awakeFromNib];
}

- (void)shiftSoundMeterLeft {
    for(int i=0; i<SOUND_METER_COUNT - 1; i++) {
        self.soundMeters[i] = self.soundMeters[i+1];
    }
}

-(void)updateMeter:(float)decibels {
    int value = (int)ceil(decibels);
    [self shiftSoundMeterLeft];
    [self shiftSoundMeterLeft];
    
    self.soundMeters[SOUND_METER_COUNT - 1] = [NSNumber numberWithInt:value];
    self.soundMeters[SOUND_METER_COUNT - 2] = [NSNumber numberWithInt:value];
    
    // The linear 0.0 .. 1.0 value we need.
    float   minDecibels = -60.0f; // Or use -60dB, which I measured in a silent room.
    if (decibels < minDecibels)
    {
        self.level = [NSNumber numberWithFloat:0.0f];
    }
    else if (decibels >= 0.0f)
    {
        self.level = [NSNumber numberWithFloat:1.0f];
    }
    else
    {
        float   root            = 2.0f;
        float   minAmp          = powf(10.0f, 0.05f * minDecibels);
        float   inverseAmpRange = 1.0f / (1.0f - minAmp);
        float   amp             = powf(10.0f, 0.05f * decibels);
        float   adjAmp          = (amp - minAmp) * inverseAmpRange;
        
        self.level = [NSNumber numberWithFloat:powf(adjAmp, 1.0f / root)];
    }
    
    self.value = [NSNumber numberWithInt:value*(-1)];
    [self setNeedsDisplay];    
}

- (void)addSoundMeterItem:(NSNotification*)lastValue {
    [self updateMeter:[lastValue.object floatValue]];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    float level = [self.level floatValue];
    int offset = [self.offset intValue];
        
    CGRect r = CGRectMake(0, 0, level*500, level*500);
    r.origin = self.bounds.origin;
    r.origin.x = self.bounds.size.width / 2  - r.size.width / 2;
    r.origin.y = self.bounds.size.height / 2  - r.size.height / 2 + offset;
    
    CGContextSetLineWidth(ctx, 10.0);
    CGContextSetRGBFillColor(ctx, 0, 0, 0.0, 0.4);
    CGContextSetRGBStrokeColor(ctx, 0, 0, 1.0, 1.0);
    CGRect circlePoint = r;
    
    CGContextFillEllipseInRect(ctx, circlePoint);
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    BOOL pointInside = NO;
    return pointInside;
}

@end
