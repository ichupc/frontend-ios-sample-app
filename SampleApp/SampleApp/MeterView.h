//
//  MeterView.h
//
//  Created by Darren Harris 17/10/2013
//  Copyright (c) 2013 Capito Systems Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SOUND_METER_COUNT       40

@interface MeterView : UIView
@property (nonatomic, retain) NSMutableArray *soundMeters;
@property (nonatomic, retain) NSNumber *value;
@property (nonatomic, retain) NSNumber *level;
@property (nonatomic, retain) NSNumber *offset;

-(void)initialise;
-(void)updateMeter:(float)decibels;

@end
